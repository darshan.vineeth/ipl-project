    /*
        matches     : Array of all matches played in all seasons of IPL from 2007 to 2017
        deliveries  : Array of all deliveries in all matches of IPL
        season      : Input season
        player      : Name of the player as input     
    
    /*  Function-1 : Matches per year
        For eg. return object = {
            "2008":58,
            "2009":57,
            "2010":60,
            "2011":73,
            "2012":74
        }
    */

    module.exports.matchesPerYear = function(matches){
        // console.log(matches)
        var matchesPerYearObject = matches.reduce(function(matchesPerYearObj,match){
            if(matchesPerYearObj.hasOwnProperty(match.season)===false){
                //If season doesnt exist, add season with count 1
                matchesPerYearObj[match.season]=1;
            }else{
                //If season already exists, increment the count
                matchesPerYearObj[match.season]++;
            }
            return matchesPerYearObj;
        },{});

        return matchesPerYearObject;
    }

    /*  Function-1 : Matches played per year/season
        For eg. return object = {
        "2008": {
            "Kolkata Knight Riders": 6,
            "Chennai Super Kings": 9,
            "Delhi Daredevils": 7,
            "Royal Challengers Bangalore": 4,
            "Rajasthan Royals": 13,
            "Kings XI Punjab": 10,
            "Deccan Chargers": 2,
            "Mumbai Indians": 7
        }
    */ 
    module.exports.matchesWonPerTeamPerYear = function(matches){

        var matchesWonPerTeamPerYearObject = matches.reduce(function(matchesWonPerTeamPerYearObj,match){
            if( match.winner !==0){

                if(matchesWonPerTeamPerYearObj.hasOwnProperty(match.season)===false){
                //If season doesn't exist, add season and add the winner
                    matchesWonPerTeamPerYearObj[match.season]={};
                    matchesWonPerTeamPerYearObj[match.season][match.winner]=1;
                
                }else if(matchesWonPerTeamPerYearObj[match.season].hasOwnProperty(match.winner)===false){
                //If season exists, but winner not exist, add the winner
                    matchesWonPerTeamPerYearObj[match.season][match.winner]=1
                }else{
                //If the winner already exist, increment the win count
                    matchesWonPerTeamPerYearObj[match.season][match.winner]++
                }

            }    
            return matchesWonPerTeamPerYearObj;
        },{});

        return matchesWonPerTeamPerYearObject;
    }

    /* Function-3 : Extra runs conceded per team in a season
        For eg: return object = {
            "Rising Pune Supergiants":108,
            "Mumbai Indians":102,
            "Kolkata Knight Riders":122,
            "Delhi Daredevils":106,
            "Gujarat Lions":98
        }
    */

    module.exports.extraRunsConcededPerTeamInASeason = function(matches,deliveries,season){
        
        //Create an array with match ids of matches in the given season
        var matchesInSeason = matches.reduce(function(matchesInSeasonArr,match){
            if(match.season == season){
                matchesInSeasonArr.push(match.id);
            }
            return matchesInSeasonArr;
        },[]);
        
        //Reducing the deliveries array to get the required output
        var extraRunsConcededPerTeamInSeasonObject = deliveries.reduce(function(extraRunsConcededPerTeamInSeasonObj,delivery){
            
            if(matchesInSeason.includes(delivery.match_id)){

                //If the delivery is from a match of the given season
                if(extraRunsConcededPerTeamInSeasonObj.hasOwnProperty(delivery.bowling_team)===false){
                    extraRunsConcededPerTeamInSeasonObj[delivery.bowling_team] = Number(delivery.extra_runs);
                }else{
                    extraRunsConcededPerTeamInSeasonObj[delivery.bowling_team] += Number(delivery.extra_runs);
                }

            }
            return extraRunsConcededPerTeamInSeasonObj;

        },{});

        return extraRunsConcededPerTeamInSeasonObject; 
    }

    /*Function-4 :  Top ten economical bowlers in a season
        For eg : outputObj = {
            "RN ten Doeschate":4,
            "J Yadav":4.14,
            "V Kohli":5.45,
            "R Ashwin":5.85,
            "S Nadeem":6.14,
            "Parvez Rasool":6.2,
            .
            .
            .
        } 
    */

    module.exports.topTenEconomicalBowlers = function(matches,deliveries,season){
        
        //Create an array with match ids of the matches in the season
        var matchesInSeason = matches.reduce(function(matchesInSeasonArr,match){
            if(match.season == season){
                matchesInSeasonArr.push(match.id);
            }
            return matchesInSeasonArr;
        },[]);

        //Create an object with bowler's runs conceded, balls and economy
        var bowlersEconomyObject = deliveries.reduce(function(bowlersEconomyObj,delivery){
            if(matchesInSeason.includes(delivery.match_id)){

                var illegalBallRuns = Number(delivery.wide_runs)+Number(delivery.noball_runs); //Wide runs + Noball runs

                if(bowlersEconomyObj.hasOwnProperty(delivery.bowler)===false){
                    bowlersEconomyObj[delivery.bowler]={};
                    bowlersEconomyObj[delivery.bowler]["runs"]= illegalBallRuns + Number(delivery.batsman_runs); //Total runs credited to bowler

                    //If the delivery goes illegal or wide, not considered as a valid ball
                    bowlersEconomyObj[delivery.bowler]["balls"]= illegalBallRuns === 0 ? 1 : 0

                }else{
  
                    bowlersEconomyObj[delivery.bowler]["runs"] += illegalBallRuns+Number(delivery.batsman_runs);
                    if(illegalBallRuns === 0){
                        
                        //Increment balls if its a valid delivery
                        bowlersEconomyObj[delivery.bowler].balls++
                    }
                }
                //Add / Modify property economy of the bowler (economy=runs/overs)
                bowlersEconomyObj[delivery.bowler]["economy"] = bowlersEconomyObj[delivery.bowler].runs / (bowlersEconomyObj[delivery.bowler].balls / 6);
            }
            return bowlersEconomyObj;
        },{});
        
        //Sort the bowlers economy list in ascending order of economy
        var sortedBowlersEconomyArray = Object.entries(bowlersEconomyObject).sort(function(a,b){
            return a[1].economy - b[1].economy; 
        });

        //Get only the first 10 bowlers from array
        var topTenBowlersIn2015Array = sortedBowlersEconomyArray.slice(0,10);
        
        
        //Output object with top 10 economical bowlers and their economy
        var topTenEconomicalBowlersIn2015Object = Object.fromEntries(topTenBowlersIn2015Array.map(function(bowlerStat){
            //bowlerStat[0] : Bowler name
            //bowler[1]     : Bowlers stats { runs, balls, economy}
            return [bowlerStat[0],Number((bowlerStat[1].economy).toFixed(2))]
        }));
        
        return topTenEconomicalBowlersIn2015Object;
    }

    /* Function-5 (extra deliverable-1) : Teams in IPL who won the toss and also won the match
    For eg : outputObj = {
        "Rising Pune Supergiant":5,
        "Kolkata Knight Riders":44,
        "Kings XI Punjab":28,
        "Royal Challengers Bangalore":35
    }
    */
    module.exports.teamWonTossAndWonMatch = function(matches){

        var teamWonTossAndWonMatchObject = matches.reduce(function(teamWonTossAndWonMatchObj,match){
            if(match.toss_winner===match.winner){
                //For each match, check if the toss_winner team won the match

                if(teamWonTossAndWonMatchObj.hasOwnProperty(match.toss_winner)===false){
                    teamWonTossAndWonMatchObj[match.toss_winner]=1
                }else{
                    teamWonTossAndWonMatchObj[match.toss_winner]++
                }   

            }
            return teamWonTossAndWonMatchObj;
        },{});

        return teamWonTossAndWonMatchObject;
    }

    /* Function-6 (extra deliverable-2) : Player of the season
        For eg: outputObj = {
            "2008": "SE Marsh",
            "2009": "YK Pathan",
            "2010": "SR Tendulkar",
            "2011": "CH Gayle",
            "2012": "CH Gayle"
        }
    */
    module.exports.playerOfTheMatchPerSeason = function(matches){

        var allPlayersPerSeasonObject = matches.reduce(function(allPlayersPerSeasonObj,match){  
            //From each match, get the player of the match and add to all player per season object

            if(allPlayersPerSeasonObj.hasOwnProperty(match.season)===false){
                
                allPlayersPerSeasonObj[match.season]={}
                allPlayersPerSeasonObj[match.season][match.player_of_match]=1

            }else if(allPlayersPerSeasonObj[match.season].hasOwnProperty(match.player_of_match)===false){

                allPlayersPerSeasonObj[match.season][match.player_of_match]=1

            }else{

                allPlayersPerSeasonObj[match.season][match.player_of_match]++
            }
            
            return allPlayersPerSeasonObj;
        },{});

        //From the list of players in each season, find the player who has won the max awards
        var playerOfTheMatchPerSeasonArray = Object.entries(allPlayersPerSeasonObject).map(function(seasonStat){
            //Get seasonStat(list of players in that season) object for each season

            let playerOfTheSeason = Object.entries(seasonStat[1]).reduce(function(playerWithAwards,playerStat){

                //Reduce the players array to get the player with max no of titles  
                if(playerStat[1]>playerWithAwards[1]){
                    return playerStat;
                }
                return playerWithAwards;

            });

            //Return an array with the season name and the player of the season
            return [seasonStat[0],playerOfTheSeason[0]];
        });
        
        var playerOfTheMatchPerSeasonObject = Object.fromEntries(playerOfTheMatchPerSeasonArray);
        
        return playerOfTheMatchPerSeasonObject;
    }
    
    /*Function-7 (extra deliverables-3) : Strike rate of a given player per season
        For eg: strikeRateofViratKohli = {
            "2008": 105.1,
            "2009": 112.33,
            "2010": 145.5,
            "2011": 121.35,
            "2012": 112
        }
    */
    module.exports.strikeRateOfPlayerPerSeason = function(matches,deliveries,player){

        //Create an object of matchId and its corresponding season
        var idObject = matches.reduce(function(idObj,match){
            if(idObj.hasOwnProperty(match.id)===false){
                idObj[match.id]=[match.season]
            }
            return idObj;
        },{});

        //Create a object with Player's runs scored and balls faced per season   
        var runsPlayerScoredPerSeasonObject = deliveries.reduce(function(runsPlayerScoredPerSeasonObj,delivery){
            if(delivery.batsman=== player){

                if(runsPlayerScoredPerSeasonObj.hasOwnProperty(idObject[delivery.match_id])===false){

                    runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]] = {}
                    runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]]["runs"] = Number(delivery.batsman_runs);

                    if(delivery.noball_runs == 0 && delivery.wide_runs == 0){
                        runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]]["balls"]=1;
                    }else{
                        runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]]["balls"]=0;
                    }

                }else{

                    runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]]["runs"] += Number(delivery.batsman_runs);
                    if(delivery.noball_runs == 0 && delivery.wide_runs == 0)
                    runsPlayerScoredPerSeasonObj[idObject[delivery.match_id]]["balls"]++
                    
                }
            }
            return runsPlayerScoredPerSeasonObj;
        },{})

        //Get strike rate
        var strikeRateOfViratPerSeasonArray =  Object.entries(runsPlayerScoredPerSeasonObject).map(function(runsInASeason){
            //runsInSeason[0] : Player name
            //runsInSeason[1] : Player stats object {runs, balls}

            return [runsInASeason[0],Number(((runsInASeason[1]["runs"] / runsInASeason[1]["balls"]) * 100).toFixed(2))]
        });
        
        //Output object
        var strikeRateOfPlayerPerSeasonObject = Object.fromEntries(strikeRateOfViratPerSeasonArray);
        return strikeRateOfPlayerPerSeasonObject;
    }

    /*Function-8 (extra deliverables-4) : Function to get the bowler who have dismissed a player most no of times
        For eg : outputObj = {
            "DA Warner": {
                "Harbhajan Singh": 3
            },
            "S Dhawan": {
                "P Kumar": 4
            }
        }
    */
    module.exports.playerDismissals = function(deliveries){

        //Get player and all the bowlers who have dismissed him
        var playerDismissalObject = deliveries.reduce(function(playerDismissalObj,delivery){

            if(delivery.player_dismissed!==0 && delivery.dismissal_kind!=='runout'){
            
                if(playerDismissalObj.hasOwnProperty(delivery.player_dismissed)===false){
            
                    playerDismissalObj[delivery.player_dismissed]={};
                    playerDismissalObj[delivery.player_dismissed][delivery.bowler]=1;
            
                }else if(playerDismissalObj[delivery.player_dismissed].hasOwnProperty(delivery.bowler)===false){
            
                        playerDismissalObj[delivery.player_dismissed][delivery.bowler]=1;
            
                    }else{
            
                        playerDismissalObj[delivery.player_dismissed][delivery.bowler]++;
            
                }
            }
            return playerDismissalObj;
        },{});

        //Reduce object with only player and the bowler who dismissed him the most no of times
        var playerDismissedByABowlerMostNosObject = Object.entries(playerDismissalObject).reduce(function(playerDismissedByABowlerMostNosObj,playerDismissal){
            
            //playerDismissal[0]    : name of the player
            //playerDismissal[1]    : Bowlers object who dismissed the player 
            var player = 'player';
            
            //Get the bowler who dismissed the player most no of times
            var bowlerDismissedMostNos = Object.entries(playerDismissal[1]).reduce(function(bowlerDismissedMostObj,bowlerStat){
                
                //bowlerStat[0] : Name of the bowler
                //bowlerStat[1] : No. of times the bowler dismissed the player
                if(bowlerStat[1] > bowlerDismissedMostObj[player]){
                    bowlerDismissedMostObj = {}
                    bowlerDismissedMostObj[bowlerStat[0]] = bowlerStat[1];
                    player = bowlerStat[0];
                }
            
                return bowlerDismissedMostObj;
            
            },{'player':-1}); // Initialising the bowlerDismissedMostObj to some minimum value 

            playerDismissedByABowlerMostNosObj[playerDismissal[0]] = bowlerDismissedMostNos;
            return playerDismissedByABowlerMostNosObj;
        
        },{});
        
        return playerDismissedByABowlerMostNosObject;
    }

    /*Function-9 (extra deliverable-5) : Economical bowler and his economy in super overs in IPL
        For eg : ["JJ Bumrah",4]
    */
    module.exports.economicalBowlerInSuperOvers = function(deliveries){

        //Get economy of all bowlers who have bowled in Super Overs
        var bowlersEconomyInSuperOversObject = deliveries.reduce(function(bowlersEconomyInSuperOversObj,delivery){

            if(delivery.is_super_over !== 0){
                //If the delivery is in a super over
                if(bowlersEconomyInSuperOversObj.hasOwnProperty(delivery.bowler)===false){
            
                    bowlersEconomyInSuperOversObj[delivery.bowler]={}
                    bowlersEconomyInSuperOversObj[delivery.bowler]["runsConceded"] = Number(delivery.wide_runs) + Number(delivery.noball_runs) + Number(delivery.batsman_runs);
            
                    if(delivery.wide_runs == 0 && delivery.noball_runs ==0){
                        bowlersEconomyInSuperOversObj[delivery.bowler]["balls"] = 1;
                    }else{
                        bowlersEconomyInSuperOversObj[delivery.bowler]["balls"] = 0;
                    }
            
                }else{
            
                    bowlersEconomyInSuperOversObj[delivery.bowler]["runsConceded"] += Number(delivery.wide_runs) + Number(delivery.noball_runs) + Number(delivery.batsman_runs);
            
                    if(delivery.wide_runs == 0 && delivery.noball_runs ==0){
                        bowlersEconomyInSuperOversObj[delivery.bowler]["balls"]++;
                    }
             
                }
             
                let runsConceded = bowlersEconomyInSuperOversObj[delivery.bowler]["runsConceded"]
                let balls = bowlersEconomyInSuperOversObj[delivery.bowler]["balls"]
                bowlersEconomyInSuperOversObj[delivery.bowler]["economy"] = Number((runsConceded / (balls / 6) ).toFixed(2));         
            
            }
            
            return bowlersEconomyInSuperOversObj;
        },{});

        //Find the economical bowler from the list
        var economicalBowlerInSuperOvers = Object.entries(bowlersEconomyInSuperOversObject).reduce(function(economicalBowlerInSuperOverArr,bowlerStat){
            // bowlerStat[0]   : Name of the bowler
            // bowlerStat[1]   : Economy of the bowler
            if(bowlerStat[1].economy < economicalBowlerInSuperOverArr[1]){

                return [bowlerStat[Object.keys(bowlerStat)[0]] , bowlerStat[1].economy]
            }
            
            return economicalBowlerInSuperOverArr;
        
        },['player', Infinity]); //Initialising the economicalBowlerInSuperOversArr with some max value 

        return economicalBowlerInSuperOvers;
    }
