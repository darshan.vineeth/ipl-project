
function iplDataPromise(){
    return new Promise((resolve,reject)=>{

        const ipl = require('./ipl.js'); //Function module
        const csvToJson = require('convert-csv-to-json'); //package to convert csv to JSON
        var fs=require('fs'); 
        
        //Get required data as objects
        const iplMatches = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv('../data/matches.csv');
        const matchDeliveries = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv('../data/deliveries.csv');
        resolve({ipl: ipl,fs : fs,iplMatches : iplMatches,matchDeliveries :matchDeliveries});

        reject();
    });
}

iplDataPromise().then(({ipl,fs,iplMatches,matchDeliveries})=>{

//Function to write JSON to file
function jsonToFile(jsonObject,fileName){
    fs.writeFile(`../output/${fileName}.JSON`,jsonObject,function(error){
        if(error){
            throw error;
        }
        console.log(`${fileName}.JSON file saved successfully!`);
    });
}   


//Function-1 : Matches per year
var matchesPerYearObject = ipl.matchesPerYear(iplMatches) //Get output object
var matchesPerYearJSON = JSON.stringify(matchesPerYearObject); //Get output JSON

jsonToFile(matchesPerYearJSON,'matchesPerYear'); //Write to file


//Function-2 : Matches won per team per season 
var matchesWonPerTeamPerYearObject = ipl.matchesWonPerTeamPerYear(iplMatches) //Get output object
var matchesWonPerTeamPerYearJSON = JSON.stringify(matchesWonPerTeamPerYearObject); //Get output JSON

jsonToFile(matchesWonPerTeamPerYearJSON,'matchesWonPerTeamPerYear'); //Write to file


//Function-3 : Extra runs conceded per team in 2016
var extraRunsConcededPerTeamIn16Object = ipl.extraRunsConcededPerTeamInASeason(iplMatches,matchDeliveries,2016); //Get output object
var extraRunsConcededPerTeamIn16JSON = JSON.stringify(extraRunsConcededPerTeamIn16Object); //Get output JSON

jsonToFile(extraRunsConcededPerTeamIn16JSON,'extraRunsConcededPerTeamIn2016'); //Write to file


//Function-4
var topTenEconomicalBowlersIn15Object = ipl.topTenEconomicalBowlers(iplMatches,matchDeliveries,2015);//Get output object
var topTenEconomicalBowlersIn15JSON   = JSON.stringify(topTenEconomicalBowlersIn15Object);//Get output JSON

jsonToFile(topTenEconomicalBowlersIn15JSON,`topTenEconomicalBowlersIn2015`); //Write to file


//Extra deliverables-1
var teamWonTossAndWonMatchObject = ipl.teamWonTossAndWonMatch(iplMatches); //Get output object
var teamWonTossAndWonMatchJSON = JSON.stringify(teamWonTossAndWonMatchObject); //Get output JSON

jsonToFile(teamWonTossAndWonMatchJSON,'teamWonTossAndWonMatch'); //Write to file


//Extra deliverable-2
var playerOfTheMatchPerSeasonObject = ipl.playerOfTheMatchPerSeason(iplMatches); //Get output object
var playerOfTheMatchPerSeasonJSON = JSON.stringify(playerOfTheMatchPerSeasonObject); //Get output JSON 

jsonToFile(playerOfTheMatchPerSeasonJSON,'playerOfTheMatchPerSeason'); //Write to file


//Extra deliverable-3
var strikeRateOfViratPerSeasonObject = ipl.strikeRateOfPlayerPerSeason(iplMatches,matchDeliveries,"V Kohli"); //Get output object
var strikeRateOfViratPerSeasonJSON = JSON.stringify(strikeRateOfViratPerSeasonObject); //Get output JSON

jsonToFile(strikeRateOfViratPerSeasonJSON,'strikeRateOfViratPerSeason'); //Write to file


//Extra deliverable-4
var playerDismissalsObject = ipl.playerDismissals(matchDeliveries); //Get output object
var playerDismissalsJSON = JSON.stringify(playerDismissalsObject); //Get output JSON

jsonToFile(playerDismissalsJSON,'playerDismissals'); //Write to file


//Extra deliverable-5
var economicalBowlerInSuperOversObject = ipl.economicalBowlerInSuperOvers(matchDeliveries); //Get output object
var economicalBowlerInSuperOversJSON = JSON.stringify(economicalBowlerInSuperOversObject); //Get output JSON

jsonToFile(economicalBowlerInSuperOversJSON,'economicalBowlerInSuperOvers'); //Write to file


}).catch((error)=>{
    console.log(error.message);
});
