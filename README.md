# IPL Project

Project to transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015

5. Number of times each team won the toss and also won the match
6. Player per season who has won the highest number of *Player of the Match* awards
7. Strike rate of the batsman Virat Kohli for each season
8. Most number of times one player has been dismissed by another player
9. Bowler with the best economy in super overs